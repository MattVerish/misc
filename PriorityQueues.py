import math

class BinaryHeap:
	elems = []
	f = lambda x, y: x > y

	def __init__(self, values, function):
		self.f = function
		for elem in values:
			self.insert(elem)
			print(self.elems)

	def insert(self, val):
		self.elems.append(val)
		self.heapify_up(-1)

	def heapify_up(self, index):
		if index == -1:
			index = len(self.elems) - 1
		parent = math.ceil(index / 2.0) - 1
		if index == 0:
			return

		if self.f(self.elems[index], self.elems[parent]):
			temp = self.elems[index]
			self.elems[index] = self.elems[parent]
			self.elems[parent] = temp
			self.heapify_up(parent)


	def heapify_down(self, index):
		if 2*index + 1 > len(self.elems) - 1:
			return
		elif 2 * index + 1 == len(self.elems) - 1:
			mIndex = 2 * index + 1
		else:
			children = 2*index + 1, 2*index + 2
			mIndex = children[0]
			if self.f(children[1], children[0]):
				mIndex = children[1]

		if self.f(self.elems[mIndex], self.elems[index]):
			temp = self.elems[index]
			self.elems[index] = self.elems[mIndex]
			self.elems[mIndex] = temp
			self.heapify_down(mIndex)

	def pop(self):
		elem = self.elems[0]
		self.elems[0] = self.elems.pop()
		self.heapify_down(0)
		return elem

	def getItems(self):
		return self.elems
