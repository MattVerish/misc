import numpy as np

class Graph:
	nodes = 0
	edges = None

	#Edges are represented as an adjacency matrix
	def __init__(self, numNodes, inputEdges=None):
		self.nodes = numNodes
		if inputEdges == None:
			self.edges = np.zeros((numNodes, numNodes))
			return
		self.edges = np.asarray(inputEdges)

	def neighbors(self, node):
		neighbors = []
		for i in range(self.nodes):
			if self.edges[node][i] >= 1:
				neighbors.append(i)
		return neighbors

	def addEdge(self, s, t, weight=1):
		if s >= self.nodes or t >= self.nodes:
			print('index out of range')
			return
		self.edges[s][t] = weight
		self.edges[t][s] = weight

	def addNodes(self, numNodes):
		self.nodes += numNodes
		self.edges = np.pad(self.edges, ((0, numNodes), (0, numNodes)), mode='constant')



