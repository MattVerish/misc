from Graph import Graph
from PriorityQueues import BinaryHeap
import tensorflow as tf

def testGraph():
	testingGraph = Graph(5)

	print(testingGraph.neighbors(1))
	testingGraph.addEdge(1, 3, 10)
	print(testingGraph.neighbors(1), testingGraph.neighbors(3))


	testingGraph.addEdge(5, 3)
	testingGraph.addEdge(3, 5)
	testingGraph.addNodes(1)
	testingGraph.addEdge(5, 3)
	print(testingGraph.neighbors(5))

def testHeap():
	#maxHeap = BinaryHeap([1, 9, 2, 7, 0], lambda x, y: x > y)
	#print(maxHeap.elems)
	#maxHeap.insert(10)
	#print(maxHeap.elems)
	#maxHeap.insert(5)
	#print(maxHeap.elems)
	#maxHeap.insert(1)
	#print(maxHeap.elems)
	#maxHeap.insert(15)
	#print(maxHeap.elems)

	#maxHeap.pop()
	#print(maxHeap.elems)

	b = [7, 8, 12, 5, 43, 0, -6, 4, 100]
	sortHeap = BinaryHeap(b, lambda x, y: x > y)
	print(sortHeap.elems)
	c = []
	for i in range(len(b) - 1):
		c.append(sortHeap.pop())
	print(c)

testHeap()

